---
layout: "image"
title: "Präziser Polarkoordinaten-Plotter"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention181.jpg"
weight: "181"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48100
- /details4567.html
imported:
- "2019"
_4images_image_id: "48100"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "181"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48100 -->
