---
layout: "overview"
title: "Veranstaltungen"
date: 2020-02-22T08:53:16+01:00
legacy_id:
- /php/categories/1301
- /categories4ab0.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1301 --> 
Immer öfter finden Treffen von fischertechnik-Fans statt. Jeder bringt sein Modell mit, es wird gefachsimpelt, und natürlich auch fotografiert. Die Fotos findet ihr hier.
