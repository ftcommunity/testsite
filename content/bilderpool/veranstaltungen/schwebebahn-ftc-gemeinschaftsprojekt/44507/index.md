---
layout: "image"
title: "Depot - Detail der Weichenveriegelung 1"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
schlagworte: ["Schwebebahn", "Depot"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44507
- /details8c02.html
imported:
- "2019"
_4images_image_id: "44507"
_4images_cat_id: "3299"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44507 -->
So werden die Weichen exakt positioniert. Ein speziell gefertigter Trichter fängt die Schubstange ein und führt den Schienenträger zur Sollposition.