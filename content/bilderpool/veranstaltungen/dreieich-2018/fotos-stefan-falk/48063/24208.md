---
layout: "comment"
hidden: true
title: "24208"
date: "2018-09-30T22:48:02"
uploadBy:
- "techum"
license: "unknown"
imported:
- "2019"
---
Bilderkennung über trainiertes Google Tensorflow Modell. 
5 verschiedene Bausteine werden erkannt und ihre Position per File an den Roboter  übergeben. Diesem kann man eingeben, welche Teile eingesammelt und in den grauen Kasten gelegt werden sollen.