---
layout: "image"
title: "Spurfolger"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention037.jpg"
weight: "37"
konstrukteure: 
- "Huub van NIekerk"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47956
- /details2e11-2.html
imported:
- "2019"
_4images_image_id: "47956"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47956 -->
