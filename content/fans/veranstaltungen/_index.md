---
title: "Veranstaltungen"
weight: 1
date: 2019-04-04T00:00:00
uploadBy:
- "EstherM"
---

Die fischertechnik-Community trifft sich regelmäßig zu Stammtischen und
Conventions sowie auf Modellbau-Messen und Maker Faires. Alle sind
herzlich eingeladen.

### Termine 

Am Samstag, 30.10.2021 wird die [Südconvention](./suedconvention2021) im [Erlebnismuseum Fördertechnik in Sinsheim](https://www.erlebnismuseum-fördertechnik.de/) stattfinden.

![Südconvention-Flyer](plakat2021-v1_5-1.png)


Am 18. Juni 2021 fand die [Maker Faire Hannover Digital Edition](https://maker-faire.de/hannover/) statt. Wir waren mit einem [Stand](./makerfaire2021) dabei!

![Maker Faire Logo](MF_ichbindabei_NEON_1200x675.jpg)

### Veranstaltungsübersicht der fischertechnik GmbH

[fischertechnik GmbH](https://www.fischertechnik.de/de-de/ueber-uns/termine)


