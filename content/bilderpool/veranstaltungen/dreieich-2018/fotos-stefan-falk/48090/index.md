---
layout: "image"
title: "Zugmaschine"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention171.jpg"
weight: "171"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48090
- /details9e2f-2.html
imported:
- "2019"
_4images_image_id: "48090"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "171"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48090 -->
