---
layout: "comment"
hidden: true
title: "20257"
date: "2015-02-26T07:00:36"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Also das mit den extra Wagen ist (egal in welcher Ausführung, wenn ich die folgenden Bilder bedenke) doch eine sehr aufwendige Sache.

Zur Kette fallen mir einige Punkte ein: Optimal für Dich wären zwei Ketten über dem Wagen. Am Fahrwerk findet sich sicher ein Punkt hinten auf jeder Seite (BS15 mit den blauen Bauplatten drauf?), an dem jeweils ein Mitnehmer anliegen kann. Ich denke beim Mitnehmer an einen BS7,5 flach auf Förderglied. Erreicht der Wagen das Ende vom Lifthill, wird er von alleine schneller und löst sich von den Mitnehmern wie ein nicht gekuppleter Eisenbahnwagen von der bremsenden Lok..

Die Ketten könntest Du an jeweils einer Umlenkrolle per Gewicht spannen. So ähnlich wie die Seilspanner bei Markus Prater-Riesenrad (http://ftcommunity.de/details.php?image_id=39484). Feder ginge eventuell auch.

Grüße
H.A.R.R.Y.