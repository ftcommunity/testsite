---
layout: "image"
title: "Pneumatik-Bagger"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention155.jpg"
weight: "155"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48074
- /detailsdd28-2.html
imported:
- "2019"
_4images_image_id: "48074"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "155"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48074 -->
