---
layout: "comment"
hidden: true
title: "20753"
date: "2015-06-22T18:42:16"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Also, BS7,5 hatte ich schon reichlich (und extra für die Achterbahn) gebunkert, die U-Träger und die roten Flachträger auch. U-Träger-Adapter und kleine Winkelsteine sind nicht so dicke da, würden aber ausreichen. Nur das Gefummel, um für jeden Bahnabschnitt den richtigen Abstand und Winkel hinzukriegen, ist mir dann doch zuviel. Dann lieber ein ft-fremdes, stabiles Innenteil, und gut ist. 

Allerdings... der  Looping hat ja gar keine Kreisbahn. Ich hab da schon etwas in Richtung einer Klothoide hingebogen, und das muss mit Fahrradfelge als Innenkörper auch wieder so herauskommen. Also läuft das so oder auf Gefummel hinaus.

Eine Alpentour muss ich nicht anfangen :-) Ich kriege im Stadtverkehr so alle 7000 km eine Felge reif für die ewigen Radgründe.

Gruß,
Harald