---
layout: "image"
title: "Positionierung"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention117.jpg"
weight: "117"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48036
- /details24e8-2.html
imported:
- "2019"
_4images_image_id: "48036"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "117"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48036 -->
Synchron dazu, wie man den großen Drehschalter am grünen Griff dreht, dreht sich auch der Rotor links.