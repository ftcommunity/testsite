---
title: "Aufgaben zum Inhalt"
weight: 100
layout: "techdoc"
stand: "18. Juni 2020"
--- 
Die Daten für unsere Webseite befinden sich in einem öffentlichen Gitlab-Repository (https://gitlab.com/ftcommunity/website-hugo-poc)
im Verzeichnis `content`. 

Wenn Du selbst Inhalte hochladen möchtest, musst du Schreibberechtigung auf diesem Repository haben.

Es gibt prinzipiell zwei Möglichkeiten, Daten und Dateien dort hochzuladen:
* Du hast lokal bei dir Git und Hugo installiert und hast ein lokales Repository.
* Du benutzt das Webinterface von Gitlab.

Die folgenden Anleitungen gehen davon aus, dass du das Repository lokal bei dir geclont hast 
und mit einem Git-Client oder über die Kommanozeile Git-Befehle ausführen kannst.
Außerdem solltest du Hugo (Version 0.66.0) bei dir lokal installiert haben.
Dann kannst du mit [Archetypes](../ftpedia/archetypes) einfach und schnell
Dateien im richtigen Format erzeugen.
Wenn du Hugo nicht installieren willst, musst du die angebebenen Dateien selbst erzeugen,
z.B. indem du existierende Dateien kopierst und die Kopien händisch anpasst.

Außerdem kannst du Hugo auf deinem Rechner selbst die Webseite bauen lassen.
Da die Webseite insgesamt sehr groß ist, dauert das einige Minuten.
Eigentlich bringt Hugo einen eigenen Webserver mit.
Mit unserer Webseite ist dieser aber bei dir lokal wahrscheinlich überfordert.
Daher musst du auch noch lokal einen Webserver betreiben.
Die fertig gestellten Seiten liegen in `<hugo-Verzeichnis>\public`.

Zu Beginn jeder Arbeit solltest dir den aktuellen Stand des Repositorys holen (`git pull`).

Mit `git add *`, `git commit` und `git push origin master:<branch>`
schiebst du die fertigen Dateien in einen Branch mit dem Namen `<branch>` in unser Repo,
wo sie dann schnellstmöglich von einem Administrator in den Hauptbranch überführt werden.
Diese Befehle musst du nach jeder unten beschriebenen Arbeit ausführen.

## Inhaltsverzeichnis

<p><a href="#einen-teaser-für-die-nächste-ft-pedia-erstellen">Anleitung: Einen Teaser für die nächste ft:pedia erstellen</a></p>
<p><a href="#eine-neue-ausgabe-einstellen">Anleitung: Eine neue ft:pedia einstellen (mit Jahrgang und Begleit-Downloads)</a></p>
<p><a href="#einen-download-zur-ft-pedia-einbauen">Anleitung: Einen Download zur ft:pedia einbauen</a></p>
<p><a href="#einen-inhalt-verschieben">Anleitung: Einen Inhalt verschieben</a></p>
<p><a href="#eine-bildergalerie-anlegen">Anleitung: Eine Bildergalerie anlegen</a></p>

## Einen Teaser für die nächste ft:pedia erstellen

Das Format des Teasers ist [hier](../ftpedia/ftpedia/#teaser) beschrieben.
In deinem Hugo-Verzeichnis (z.B. "website-hugo-poc") führst Du folgenden Befehl aus,
um einen Teaser für das nächste Heft anzulegen:

`hugo new -k teaser ftpedia/2020/2020-2/_index.md`.

Du bist sicher selbständig in der Lage, in diesem Befehl das richtige Jahr und die richtige Nummer
der Ausgabe einzusetzen, und dabei `-` und `_` korrekt zu setzen
In dem entstandenen Verzeichnis speicherst du das Vorschaubild der Titelseite unter
`titelseite.png` ab.
In der Datei `_index.md` setzt du noch das `launchDate` auf das Datum,
an dem die ft:pedia erscheinen wird. 
Beim Format orientierst du dich am besten an dem Eintrag von `date`.

Vor der ersten Ausgabe eines Jahres musst du noch die Übersicht für das Jahr anlegen 
mit dem Befehl:

`hugo new -k year ftpedia/2020/_index.md`.

## Eine neue Ausgabe einstellen

Das Format einer ft:pedia-Ausgabe ist [hier](../ftpedia/ftpedia/#einzelheft) beschrieben.

Vor dem Hochladen eines neuen Heftes musst Du den bestehenden Teaser (`_index.md`) löschen.

In deinem Hugo-Verzeichnis führst Du dann folgenden Befehl aus,
um die beschreibende Datei für das nächste Heft anzulegen:

`hugo new ftpedia/2020/2020-2/_index.md`

In der neuen Datei musst du `publishDate` auf das Datum setzen,
an dem die ft:pedia erscheinen wird. Hugo wird die Seite erst an diesem Tag bauen.
Du könntest also die ft:pedia schon vorher hochladen, aber dann hättest du keinen Teaser.

Das Heft-PDF (`YYY-n.pdf`) und die Titelseite (`titelseite.png`) speicherst Du in diesem Verzeichnis.
Außerdem legst du die Artikelübersicht (`ftPedia_Artikeluebersicht.csv`) direkt in dem Verzeichnis `ftpedia` ab.

## Einen Download zur ft:pedia einbauen

Zu jeder heftbegleitenden Download-Datei gehört eine beschreibende Markdown-Datei.
Das Format dieser Datei ist [hier](../knowhow/download) beschrieben.
Der Dateiname der Download-Datei soll keine Leerzeichen, keine Klammern oder sonstige Sonderzeichen enthalten.

Die Begleitdatei legst du mit

`hugo new -k download ftpedia/2020/2020-2/dateiname.md`

an. Als `dateiname` wählst du den Namen der Download-Datei, ohne die Endung für den Typ.
In der Markdown-Datei trägst du noch bei `file` die Endung nach, damit der Dateiname korrekt ist.
Außerdem ergänzt du noch den oder die Autoren (unter `konstrukteure`).
Einen beschreibenden Text schreibst du unter die Striche, die das Frontmatter abschließen.
Wenn dir der Titel (`title`) nicht gefällt, kannst du ihn auch ändern.

Der wichtigste Schritt ist jetzt, die Download-Datei selbst in dem Verzeichnis abzuspeichern!

Auf die gleiche Weise kannst du auch eine Download-Datei in einer anderen Kategorie,
z.B. im Know-How-Bereich anlegen. 
Wenn du dafür ein neues Unterverzeichnis anlegst, muss dieses auch eine Datei mit dem Namen
`_index.md` enthalten. Diese Datei sieht dann z.B. so aus:

```
---
title: "ftDesigner Dateien"
weight: 70
---
Für das Programm ["ftDesigner"](http://www.3dprofi.de/de/) gibt es zahlreiche interessante Modelle.
```

Der Austausch einer Download-Datei, z.B. nach der Korrektur eines Fehlers,
ist unproblematisch:
die neue Datei wird einfach an Stelle der alten abgespeichert.

## Einen Inhalt verschieben

Da Hugo auf Dateien und Verzeichnissen basiert, ist es ganz einfach,
Inhalte auf der Webseite zu verschieben.
Du verschiebst die entsprechende Datei oder das entsprechende Verzeichnis 
in deinem lokalen Hugo-Verzeichnis an die gewünschte Stelle. 
Wenn du einen neuen Platz für eine Download-Datei brauchst,
musst du unbedingt daran denken, Markdown-Datei und die Datei selbst
gemeinsam zu verschieben.
Außerdem musst du, wie oben beschrieben, daran denken,
dass in jedem Verzeichnis eine `_index.md`-Datei liegen muss.

## Eine Bildergalerie anlegen

Eine Bildergalerie ist eine [relative komplexe Sache](../bilderpool).
Daher haben wir Skripte programmiert,
die die nötigen Dateien halbautomatisch anlegen.
Am einfachsten geschieht der Upload von Bildern 
mit [Stefan Falks ft-Publisher](https://www.ct-systeme.com/Seiten/go-publisher.aspx).
Voraussetzung sind Python3 und Hugo, die du lokal installiert haben musst..

Das geht so:

1. Das Publisher-Projekt "wie gewohnt" erzeugen, mit Bildern und Texten versehen. Wer Microsoft Word installiert hat, kann auch die Rechtschreibkorrektur nutzen, bevor veröffentlich wird.

2. "Veröffentlichen", aber nicht zur ft Community-Website, sondern ins Dateisystem (das ist einer der wählbaren Veröffentlichungs-Zielen: ftc, Dateisystem, html, Word, PowerPoint). Das ergibt standardmäßig einen Unterordner "published" unterhalb der Projektdatei, und in dem landen die gerenderten Bilder. Ihr könnt aber auch jeden anderen Ordner als Ziel angeben. Die so gespeicherten Bilder sind genau dieselben, die auch beim automatischen Hochladen verwenden würden. Da sind also alle Bildbearbeitungen wie Drehen, Beschneiden, Schärfen, Helligkeit, Kontrast, Gammakorrektur abgearbeitet.

3. Alle Bilder dieses Ordners plus die Publisher-Datei (die .ftcpub-Datei, da sind die erfassten Texte und die Bildreihenfolge drin) zusammen in einem Verzeichnis speichern.

4. In diesem Verzeichnis (dem Upload-Verzeichnis) die Daten aus der Publisher-Datei in eine CSV-Datei schreiben. 
Dafür dient das Skript [readftpub.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/readftpub.py).
Der Aufruf erfolgt mit `python3 readftpub.py <Upload-Verzeichnis> <Name>.ftcpub`
Danach überprüfst du die CSV-Datei, ob alles richtig ist. Gelegentlich müssen die Dateinamen der Bilder noch korrigiert werden.

5. Für den nächsten Schritt brauchst du Hugo. 
Das Python-Skript [upload-from-csv.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/upload-from-csv.py)
ruft Hugo-Befehle auf, um die nötigen Dateien zu erzeugen.
Hugo legt dann eine neue Galerei in dem angebebene Ziel-Verzeichnis, z.B. "/bilderpool/modelle/bauwerke", an.
Da die Bilder im Moment noch fortlaufend nummeriert sind, 
musst du noch die Bildnummer des bis jetzt neuesten Bildes angeben.
Du findest sie auf der Startseite von ftcommunity.de. 
Es ist die 5-stellige Zahl im Verzeichnisnamen des dort gezeigten Bildes.
Du rufst das Skript in deinem Hugo-Verzeichnis (z.B. "website-hugo-poc") mit
`python3 upload-from-csv.py <Upload-Verzeichnis> <Ziel-Verzeichnis> <Bildnummer>` auf.

Bevor du die Bilder in das ftcommunity-Repository hochlädst, solltest du dringend prüfen,
ob alles so aussieht, wie du dir das vorgestellt hast.

Wenn dir der beschriebene Prozess zu kompliziert ist, 
kannst du die mit Schritt 3 erstellte Datei auch einfach an website-team@ftcommunity.de schicken.

Wenn du den Publisher nicht verwenden willst, erstellst du die csv-Datei selbst.

Diese CSV-Datei hat folgende Felder:
- "Titel"
- "Beschreibung"
- "Fotografen"
- "Konstrukteure",
- "weight"
- "picture"
Jedes Bild kommt in eine Zeile. 

In die Spalte "picture" kommt jeweils der Dateinamen des Bildes.
Außerdem muss es noch die beiden Dateien `Titel` und `Beschreibung` für den Titel und die Beschreibung der Galerie geben.

Dann machst du entweder mit Schritt 5 weiter oder schickst die Dateien und die Bilder an die angegebene Adresse.









