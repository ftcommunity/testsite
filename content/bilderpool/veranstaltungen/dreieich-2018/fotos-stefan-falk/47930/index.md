---
layout: "image"
title: "Synchrotron"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention011.jpg"
weight: "11"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47930
- /details2ed7.html
imported:
- "2019"
_4images_image_id: "47930"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47930 -->
