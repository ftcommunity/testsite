---
title: Maker Faire Hannover
layout: "event"
date: 2021-05-28T17:50.00:00+01:00
uploadBy: Website-Team
location: "Digital Edition"
termin: 2021-06-18T00:00:00+01:00
flyer: MF_ichbindabei_NEON_1200x675.jpg
---

Am 18. Juni 2021 findet die [Maker Faire Hannover Digital Edition](https://maker-faire.de/hannover/) statt.

Wir sind mit einem Stand dabei.

![Screenshot Vorschau](../makerfaire2021-stand.png)

Kommt uns doch auf unserem Stand besuchen!

Tickets gibt es kostenlos hier: https://app.vystem.io/event/maker-faire/signup
