---
layout: "file"
hidden: true
title: "DCF77-Decoder v2.0"
date: "2017-02-03T00:00:00"
file: "dcf77decoderv2.0.rpp"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dcf77decoderv2.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dcf77decoderv2.0.rpp -->
Version 2.0 des DCF77-Decoders (ft-Funkuhr); zeigt im TX-Display Uhrzeit (Stunden:Minuten, Zeitzone) und das Datum (Wochentag, Tag.Monat.Jahr) an. Synchronisiert sich stündlich neu. Ein ausführlicher Artikel dazu erscheint in ft:pedia 3/2012.