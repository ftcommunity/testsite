---
hidden: true
layout: "issue"
title: "2 / 2012"
file: "ftpedia-2012-2.pdf"
publishDate: 2012-06-30T00:00:00
date: 2012-06-30T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2012-2.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-2.pdf -->
