---
layout: "comment"
hidden: true
title: "24227"
date: "2018-10-06T10:57:37"
uploadBy:
- "vleeuwen"
license: "unknown"
imported:
- "2019"
---
Two of my boats models, a single hull and a two hull.
and 2 fischertechnik models from the .30480 crane ( 1984) and the 30474 Telekop-Mobilkran ( 1983 )
http://www.fischertechnik-museum.ch/museum/displayimage.php?album=5&pos=57
http://www.fischertechnik-museum.ch/museum/displayimage.php?album=67&pos=32
http://www.fischertechnik-museum.ch/museum/displayimage.php?album=5&pos=53
http://www.fischertechnik-museum.ch/museum/displayimage.php?album=67&pos=29