---
layout: "file"
hidden: true
title: "DCF77-Decoder v2.1"
date: "2017-02-03T00:00:00"
file: "dcf77decoderv2.1.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dcf77decoderv2.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dcf77decoderv2.1.zip -->
Version 2.1 des DCF77-Decoders (ft-Funkuhr) zeigt neben der Anzeige im TX-Display die Uhrzeit (Stunden:Minuten) und das Datum (Tag.Monat.Jahr) auf drei I²C-LED-Displays (SAA1064) an. Details siehe Artikel in ft:pedia 3/2012 und Fotos von der Convention 2012.