---
title: "Tischtennisball-Weitergabe"
weight: 20
---

Zur Convention 2017 wurde eine alte Idee wiederbelebt. Hier finden sich die
Definitionen zum [Thread im Forum](https://forum.ftcommunity.de/viewtopic.php?f=6&t=3984).

Wer mag, kann die Vorgaben natürlich auch für sein eigenes Gemeinschaftprojekt
verwenden. Viel Spaß.
