---
layout: "image"
title: "Baufahrzeug"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention163.jpg"
weight: "163"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48082
- /detailsc94f-2.html
imported:
- "2019"
_4images_image_id: "48082"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "163"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48082 -->
