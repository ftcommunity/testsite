---
layout: "image"
title: "Schoko-BS30 (4)"
date: "2013-04-02T15:32:14"
picture: "P1070854_verkleinert.jpg"
weight: "4"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/36822
- /details9bda.html
imported:
- "2019"
_4images_image_id: "36822"
_4images_cat_id: "2733"
_4images_user_id: "1028"
_4images_image_date: "2013-04-02T15:32:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36822 -->
Nach dem (äußerst vorsichtigen!) Herausziehen des Verbinders liegt die erste Nut in der Schokolade frei.