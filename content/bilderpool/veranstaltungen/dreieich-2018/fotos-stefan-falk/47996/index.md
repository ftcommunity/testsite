---
layout: "image"
title: "Baustein-Sortier-Anlage"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention077.jpg"
weight: "77"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47996
- /detailsabe8.html
imported:
- "2019"
_4images_image_id: "47996"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47996 -->
Hier der Server, auf dem die KI läuft.