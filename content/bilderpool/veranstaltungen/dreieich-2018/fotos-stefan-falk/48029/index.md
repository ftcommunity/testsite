---
layout: "image"
title: "Turmbergbahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention110.jpg"
weight: "110"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48029
- /details3ed8.html
imported:
- "2019"
_4images_image_id: "48029"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "110"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48029 -->
Ralf Geerkens wunderschönes Modell reizte mich zum Nachbau. Ralfs Schienen sind viel feiner ausgetüftelt und er hat auch eine richtige Lenkung unter seinen Wagen. Das ist hier alles völlig unzulässig vereinfacht, dafür gibts automatisch öffnende Türen.