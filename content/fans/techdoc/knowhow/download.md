---
title: "Download-Datei"
topic: "Download-Datei"
layout: "techdoc"
weight: 10
stand: "10. Juni 2020"
---

## Datei mit "layout: file"

Das Dateiformat der Download-Datei kann alles sein, auch ein Bild
(wobei ein Bild eher in den Bilderpool-Bereich gehört).

Zu jeder Download-Datei gibt es eine zugehörige Markdown-Datei 
mit Beschreibung und Info zur Darstellung.

Aus organisatorischen Gründen sind die Download-Datei und die Datei mit der
Beschreibung namensgleich und unterscheiden sich nur in der Datei-Endung.
Das muss allerdings nicht zwingend so sein.
Ein Admin freut sich allerdings über sofort sichtbare Bezüge.
Die Beschreibung ist immer in einer .md-Datei untergebracht.
[_Das geht genau so lange gut, bis jemand mal eine Markdowndatei mit der
Endung .md hochlädt._]
Auf der Platte sieht das dann etwa so aus:
````
$ ls
grundbaustein.md
grundbaustein.pdf
flipflop.md
flipflop.pdf
````
grundbaustein.md enthält die Beschreibung zum Download grundbaustein.pdf und
flipflop.md enthält die Beschreibung zum Download flipflop.pdf.


### Download-Datei Beschreibung (.md)

Zuerst haben wir den "Frontmatter"-Abschnitt.
Er enthält diese Felder (die Reihenfolge ist nicht so wichtig, aber eine
einheitliche Anordnung macht es den Admins leichter):

<table>
   <tr>
      <th>Frontmatter</th>
      <th>Angabe</th>
      <th>Erläuterung</th>
   </tr>
   <tr>
      <td><b>---</b></td>
      <td></td>
      <td>Leitet den Frontmatter-Block ein</td>
   </tr>
   <tr>
      <td><b>layout</b>: "file"</td>
      <td><b>Pflicht</b></td>
      <td>Das Layout für einen Download-Eintrag heißt "file".</td>
   </tr>
   <tr>
      <td><b>title</b>: "Flip Flop Baustein"</td>
      <td><b>Pflicht</b></td>
      <td>Der Titel, der als Überschrift oben erscheint</td>
   </tr>
   <tr>
      <td><b>date</b>: 2005-06-06T00:00:00+0200</td>
      <td><b>Pflicht</b></td>
      <td>
         Das Datum des uploads - wird automatisch erzeugt.
         <br/>Codierung: yyyy-mm-dd<b>T</b>hh:mm:ss+hhmm<br/>
         Beim manuellem Erstellen neuer Seiten bitte die Zeitzone (`+0100` MEZ
         bzw. `+0200` MESZ nicht vergessen)!
         Es wird empfohlen, vorhandene Archetypes zu nutzen. Hugo kümmert sich
         dann um das korrekte Datum mitsamt der Zeitzone. Für legacy-Dateien,
         die älter als 2 h sind, ist die Angabe der Zeitzone unerheblich und
         kann entfallen. Für alles Aktuelle soll sie rein; sonst wird die Seite
         von Hugo nicht (zur richtigen Zeit) gebaut.
      </td>
   </tr>
   <tr>
      <td><b>file</b>: "flipflop.pdf"</td>
      <td><b>Pflicht</b></td>
      <td>
         Das zugehörige Download-File. Vorzugsweise unterscheiden sich die Namen von
         Download-File und Markdownfile nur in der Endung. Das hilft den menschlichen Admins sehr zu erkennen, was zusammen gehört. Beispiel: flipflop.pdf <=> flipflop.md
      </td>
   </tr>
   <tr>
      <td><b>konstrukteure</b>:<br />- "Michael Becker"</td>
      <td>Option</td>
      <td>
         'Autoren' wäre hier von der Wortwahl geschickter, 'konstrukteure' ist
         technisch aber das gleiche. Also nennen wir es genau so, wie es auch
         im Bilderpool benannt ist. Für den Fall der Fälle (wir haben tatsächlich
         welche) ist das als Liste mit mehreren Einträgen vorbereitet. Je Autor
         eine Zeile und bitte das "-" davor!
         Mittlerweile ist die zugehörige Steuerdatei für die Darstellung in
         der Lage, auch mit leeren Listen umzugehen. Trotzdem bitte bei
         manuellen Pflegearbeiten darauf achten, hier entweder den Eintrag mit
         wenigstens einem Autor zu machen oder die Zeile komplett weg zu lassen.
      </td>
   </tr>
   <tr>
      <td><b>uploadBy</b>: <br />- "Michael Becker"</td>
      <td><b>Pflicht</b></td>
      <td>
         Logisch macht es keinen Sinn hier eine Liste zu führen, denn der
         upload erfolgt natürlich nur von einem Nutzer. Nun gibt es da aber
         eine kleine Kosmetik für eine angenehme Formulierung der Angaben auf
         der Seite. Und diese Kosmetik funktioniert (derzeit) nur mit gleichen
         Datentypen. Deswegen: Liste mit <b>nur einem</b> Eintrag.
         Mittlerweile ist das zugehörige Kontrollfile für die Darstellung in
         der Lage, auch mit leeren Listen umzugehen. Trotzdem bitte bei
         manuellen Pflegearbeiten darauf achten, hier entweder den Eintrag mit
         exakt einem Eintrag zu machen oder die Zeile komplett weg zu lassen.  
         `-LegacyAdmin-` ist reserviert für Dateien, bei denen nicht
         herauszufinden ist, wer den Upload gemacht hat (exklusiv für Altlasten aus der ftc vor 2019).
       </td>
   </tr>
   <tr>
      <td><b>license</b>: "unknown"</td>
      <td>Option</td>
      <td>
         Die Angabe der Lizenz zur Veröffentlichung. Gab es in der alten
         ftc nicht, ist neu hier. 
         Wenn keine Angabe gemacht wird, gilt automatisch "Alle Rechte vorbehalten"
         und die Rechte liegen beim Autor.
      </td>
   </tr>
   <tr>
      <td><b>legacy_id</b>:<br />
         - /data/downloads/ebausteine/schaltplne/flipflop.pdf
      </td>
      <td>Option</td>
      <td>
         Das ist gedacht für Seiten, die aus der alten ftc übernommen wurden.
         Ebenso nützlich aber auch, wenn mal was verschoben wurde. Dann bleiben
         die Links darauf alle wirksam.
         'https://www.ftcommunity.de' wird rausgeworfen, der Rest kommt so wie
         in der url-Zeile vom Browser. Original hieß das mal<br />
         https://www.ftcommunity.de/data/downloads/ebausteine/schaltplne/flipflop.pdf
         <br />
         Falls ein <i>imported</i> tag vergeben ist, muss auch eine 
         <i>legacy_id</i> vorhanden sein!
      </td>
   </tr>
   <tr>
      <td><b>imported</b>:<br />
         - "2019"
      </td>
      <td>Option</td>
      <td>
         Wird dieser Eintrag gemacht, ist die Datei aus einer vorherigen
         ftc-Version übernommen worden. "2019" steht dann für das Jahr, in
         dem der Import gemacht wurde.
         <br />
         Stammt der Download aus der aktuellen Version der ftc, wird dieser
         Eintrag nicht gemacht!
         <br />
         Die Idee ist, hier bei Bedarf einen Automaten drauf loszulassen der
         die Einträge vornimmt / ergänzt (z. B.: - "2019" -> - "2019" - "2033").
         Vorläufig wird das Feld nicht ausgewertet, hilft uns aber eines Tages
         herauszufinden, was schon vorher da war. <i>legacy_id</i> ist dafür
         ungeeignet, da dort <u>jede</u> ehemalige ID reinkommt - also auch
         wenn innerhalb der aktuellen ftc-Seite was verschoben wird.
         Zukunftssicher ist eine Liste besser geeignet als ein einzelner
         String.
      </td>
   </tr>
   <tr>
      <td><b>---</b></td>
      <td></td>
      <td>Schließt den Frontmatter-Block ab.</td>
   </tr>
</table>

Nach dem Frontmatter kommt nun die Beschreibung der Download-Datei.

Für das gewählte Beispiel (flipflop.md) haben wir diesen Dateiinhalt:

````
---
layout: "file"
title: "Flip Flop Baustein"
date: 2005-06-06T00:00:00
file: "flipflop.pdf"
konstrukteure: 
- "Michael Becker"
uploadBy:
- "Michael Becker"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/schaltplne/flipflop.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/schaltplne/flipflop.pdf -->
Schaltplan "Flip Flop Baustein" (36479)
````
In der Beschreibung steht der vollständige Link zur alten ftc
als html-Kommentar, um notfalls die volle Info über die Herkunft zur
Verfügung zu haben.
Der Link selbst erscheint nirgends.
Der alte Link ist natürlich nur für Seiten aus der alten ftc sinnvoll.

Die Beschreibung darf, mit allem was
[Markdown](https://learn.netlify.com/en/cont/markdown/)
bietet, formatiert werden.  
_Da wäre dann die Frage wie das der geneigte Benutzer ohne entsprechende Kenntnisse
im Upload hinbekommt? Zumindest für Admins aber zumindest schon mal wichtig._

Das Beispiel stammt von einem der Silberlinge:  
**/knowhow/elektronik/silberlinge/flipflop/**

Wie das in der Darstellung genau aussieht, bestimmt die zugehörige "Beschreibung"
in der Datei  
**/layouts/_default/file.html**,  
die die Angaben für das _layout: "file"_ liest und für eine schöne
Browseransicht umsetzt. Dieses Skript für die Darstellung ist 
[hier](../file-html) beschrieben.

