---
title: "Default-Templates"
topic: "globalen Templates"
layout: "techdoc"
stand: "2. Juli 2020"
---

## Globale Templates

Templates (= Schablonen) sind oft für die ganze Seite gültig.
Derart globale Templates erwartet Hugo im Verzeichnis `/layouts/_default`.

Aktuell haben wir hier

*  baseof.html
*  [file.html](../../knowhow/file-html)
*  list.html
*  single.html

Außerdem gibt es direkt in `layouts` das Template [index.html](../home_index),
das für das Layout der Startseite verantwortlich ist.

## Partials

Partials sind so eine Art Unterprogramm, oder hier wohl eher Code-Fragmente,
die nutzbringend an mehreren Stellen eingesetzt werden können.
Partials stehen grundsätzlich im Verzeichnis `/layouts/partials`
oder seinen Unterverzeichnissen.

Einige davon kommen schon mit Hugo, andere haben wir selbst geschrieben.

*  [download-icon.html](../../partials/doku_download-icon/)
*  [download-size.html](../../partials/doku_download-size/)
*  [menu.html](../../partials/doku_menu)
*  [menu-ftpedia-youngest.html](../../partials/docu_menu-ftpedia-youngest)

