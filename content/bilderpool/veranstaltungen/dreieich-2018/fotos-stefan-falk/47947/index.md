---
layout: "image"
title: "Demonstration stehender Wellen"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention028.jpg"
weight: "28"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47947
- /details9587-2.html
imported:
- "2019"
_4images_image_id: "47947"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47947 -->
