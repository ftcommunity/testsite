---
title: "Wiki-Seiten"
topic: "Wiki-Seite"
layout: "techdoc"
weight: 30
stand: "14. Juni 2020"
---

## Wiki-Artikel (Datei mit "layout: wiki")

Im Bereich "Know-How" gibt es neben zahlreichen Download-Dateien auch
einzelne Artikel. Die meisten davon wurden aus dem früheren Wiki von
der alten Seite importiert. Daher heißt dieses Layout hier "wiki".
Die Artikel bestehen aus einer einzelnen Datei, können aber auch Bilder 
eingebettet haben.

Für den Wiki-Artikel ist also nur eine Datei erforderlich.
Selbstverständlich bietet sich auch hier das benutzte Markdown-Format
zur Darstellung des Inhaltes an.

Und ebenso wie sonst auch ist hier zuerst ein Frontmatter enthalten, mit
dem die nötigen Informationen zur Darstellung an Hugo gegeben werden.
Es unterscheidet sich ein klein wenig von dem der [Download-Dateien](../download),
da die Angabe über eine beigeordnete Datei entfällt.

<table>
   <tr>
      <th>Frontmatter</th>
      <th>Angabe</th>
      <th>Erläuterung</th>
   </tr>
   <tr>
      <td><b>---</b></td>
      <td></td>
      <td>Leitet den Frontmatter-Block ein</td>
   </tr>
   <tr>
      <td><b>layout</b>: "wiki"</td>
      <td><b>Pflicht</b></td>
      <td>Das Layout für einen Wiki-Artikel heißt "wiki".</td>
   </tr>
   <tr>
      <td><b>title</b>: "RC5 Codes für IR Contol Set 30344"</td>
      <td><b>Pflicht</b></td>
      <td>Der Titel, der als Überschrift oben erscheint</td>
   </tr>
   <tr>
      <td><b>date</b>: 2016-08-24T00:00:00+0200</td>
      <td><b>Pflicht</b></td>
      <td>
         Das Datum des uploads - wird automatisch erzeugt.
         <br />Codierung: yyyy-mm-dd<b>T</b>hh:mm:ss+hhmm<br />
         Beim manuellem Erstellen neuer Seiten bitte die Zeitzone (`+0100` MEZ
         bzw. `+0200` MESZ nicht vergessen)!
         Es wird empfohlen, vorhandene Archetypes zu nutzen. Hugo kümmert sich
         dann um das korrekte Datum mitsamt der Zeitzone. Für legacy Dateien
         die älter als 2 h sind, ist die Angabe der Zeitzone unerheblich und
         kann entfallen. Für alles aktuelle soll sie rein, sonst wird die Seite
         von Hugo nicht (zur richtigen Zeit) gebaut.
      </td>
   </tr>
   <tr>
      <td><b>konstrukteure</b>:<br />- "uffi"</td>
      <td>Option</td>
      <td>
         'Autor' (im Singular!) wär hier von der Wortwahl geschickter,
         <i>konstrukteure</i> nennen wir es sonst aber auch.
         Im Falle eines aus der alten Seite importierten Wiki-Artikels 
         sind der Autor und der Nutzer
         normalerweise identisch.
        </td>
   </tr>
   <tr>
      <td><b>uploadBy</b>: <br />- "uffi"</td>
      <td><b>Pflicht</b></td>
      <td>
         Logisch macht es keinen Sinn, hier eine Liste zu führen, denn der
         upload erfolgt natürlich nur von einem Nutzer. Nun gibt es da aber
         eine kleine Kosmetik für eine angenehme Formulierung der Angaben auf
         der Seite. Und diese Kosmetik funktioniert (derzeit) nur mit gleichen
         Datentypen. Deswegen: Liste mit <b>nur einem</b> Eintrag.
      </td>
   </tr>
   <tr>
      <td><b>license</b>: "unknown"</td>
      <td>Option</td>
      <td>
         Die Angabe der Lizenz zur Veröffentlichung. Gab es in der alten
         ftc nicht, ist neu hier.
      </td>
   </tr>
   <tr>
      <td><b>legacy_id</b>:<br />
         - /wiki1b8f.html
      </td>
      <td>Option</td>
      <td>
         Das ist gedacht für Seiten, die aus der alten ftc übernommen wurden.
         Ebenso nützlich, aber auch wenn mal was verschoben wurde. Dann bleiben
         die Links darauf alle wirksam.
         'https://www.ftcommunity.de' wird rausgeworfen, der Rest kommt so wie
         in der url-Zeile vom Browser bis zum ersten Sonderzeichen - meist ein
         '?'.
         Original hieß das mal<br />
         https://www.ftcommunity.de/wiki1b8f.html?action=show&topic_id=40
         <br />
         Falls ein <i>imported</i> tag vergeben ist, muss auch eine 
         <i>legacy_id</i> vorhanden sein!
      </td>
   </tr>
   <tr>
      <td><b>imported</b>:<br />
         - "2019"</td>
      <td>Option</td>
      <td>
         Wird dieser Eintrag gemacht, ist die Datei aus einer vorherigen
         ftc-Version übernommen worden. "2019" steht dann für das Jahr, in
         dem der Import gemacht wurde.
         <br />
         Ist die Datei neuer als aus dem Jahr 2018, wird dieser Eintrag nicht gemacht.
         <br />
         Die Idee ist, hier bei Bedarf einen Automaten drauf loszulassen, der
         die Einträge vornimmt / ergänzt (z. B.: - "2019" -> - "2019" - "2033").
         Vorläufig wird das Feld nicht ausgewertet, hilft uns aber eines Tages
         herauszufinden, was schon vorher da war. <i>legacy_id</i> ist dafür
         ungeeignet, da dort <u>jede</u> ehemalige ID reinkommt - also auch
         wenn innerhalb der aktuellen ftc-Seite was verschoben wird.
         Zukunftssicher ist eine Liste besser geeignet als ein einzelner
         String.
      </td>
   </tr>
   <tr>
      <td><b>---</b></td>
      <td></td>
      <td>Schließt den Frontmatter-Block ab.</td>
   </tr>
</table>

Für das gewählte Beispiel (ex_wiki_RC5_Codes_fr_IR_Contol_Set_30344.md)
haben wir dieses Frontmatter:

````
---
layout: "wiki"
title: "RC5 Codes für IR Contol Set 30344 (für den schwarzen Empfänger)"
date: 2016-08-24T00:00:00
konstrukteure: 
- "uffi"
uploadBy:
- "uffi"
license: "unknown"
legacy_id:
- /wiki1b8f.html
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/wiki1b8f.html?action=show&topic_id=40 -->
<!--
   Wiki

   Thema: RC5 Codes für IR Contol Set 30344 (für den schwarzen Empfänger)

   Aktuelle Version

   von: uffi

   am: 24.08.2016, 19:04:16 Uhr

   Historie:
   Version 3 von uffi am 24.08.2016, 18:02:57 Uhr
   Version 2 von uffi am 24.08.2016, 15:23:26 Uhr
   Version 1 von uffi am 24.08.2016, 14:56:29 Uhr

Das hier wäre dann Version 4, die auf .md umgestellt und etwas aufgehübscht
wurde. Uffi wird es mir wohl verzeihen.
-->
````
Nach dem Frontmatter kommt nun noch ein wenig Antiquarisches.

In der Beschreibung steht zuerst der vollständige Link zur alten ftc
als html-Kommentar, um notfalls die volle Info über die Herkunft zur
Verfügung zu haben.
Der Link selbst erscheint nirgends.
Zusätzlich ist noch die Historie angegeben, so wie sie die alte Seite
aufgelistet hat. Die wird auch nicht angezeigt (wohl aber dem Kundigen
offenbart).

Nach diesem langen HTML-Kommentar kommt dann der eigentliche Inhalt der
Seite. Die Beschreibung darf mit allem, was
[Markdown](https://daringfireball.net/projects/markdown/syntax)
bietet, formatiert werden.
Eventuell steht an der ein oder anderen Stelle ein HTML-Tag dazwischen um
dem Markdown etwas nachzuhelfen.

Wie das alles in der Darstellung genau aussieht, bestimmt die zugehörige
"Beschreibung" in der Datei  
**layouts/knowhow/wiki.html**,  
die die Angaben für das _layout: "wiki"_ liest und in eine angenehme
Browseransicht umsetzt. Dieses Skript ist [hier](../wiki-html) beschrieben.

