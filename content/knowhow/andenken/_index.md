---
title: "Andenken"
weight: 1
---
Im Jahre 2018 wurde aus Spaß plötzlich ernst. Die jahrelang klaglos laufende
ftc-Seite ist technisch veraltet und die ewig lange Planung die Seite zu
erneuern wird plötzlich bitter notwendig.

Im Zuge der "Aktion Phönix" werden alle Inhalte der alten Seite redaktionell
überarbeitet und neu sortiert. Wir wollen das "alte" Know-how für die Nachwelt
erhalten.

In dem ganzen Fundus sind aber auch Fundsachen, die zeitlich nicht mehr aktuell
sind oder aus anderen Gründen nicht irgendwo anders unterkommen. Für diese
reinen "Andenken" gibt es diese ruhige Ecke hier.
