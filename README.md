# Testdaten für die ftc-Technik

Zur Weiterentwicklung und zum Test der "Technik" (hugo, Scripte und css)
erweist sich die Originalseite als zu groß. Dies hier ist der Versuch
mit möglichst wenig Dateimenge auszukommen.

Technisch entspricht der Inhalt und die Dateistruktur der produktiven Seite,
allerdings reduziert auf wenige Dateien aus jeder Ecke. Ein wenig ft:pedia,
ein paar Bilder, etwas Knowhow und die Techdoc. Dazu irgendein Nonsens um ein
paar spezielle Testfälle zu haben.



## Wie man sich das Repo holt und für `hugo serve` funktionsfähig bekommt
(git bash!)

````
$ cd <wo das Repo lokal liegen soll>
$ git clone https://gitlab.com/ftcommunity/testsite.git
$ cd testsite
$ git submodule update --init --recursive
````

Das Repo wird unter seinem git-Namen `testsite` angelegt. Was das gitlab
in der Übersicht anzeigt ist ein späterer Namenswechsel.

Ein `hugo serve` erlaubt nun die Inpektion der Testseite via localhost. hugo
erzählt wohin man den Browser schicken muss. Bei mir ist es derzeit
//localhost:1313/
````
$ cd <wo das Repo lokal liegt>
$ cd testsite
$ hugo serve
Building sites … WARN 2019/10/03 17:36:20 .File.UniqueID on zero object. Wrap it in if or with: {{ with .File }}{{ .UniqueID }}{{ end }}

                   | EN
+------------------+-----+
  Pages            | 104
  Paginator pages  |   0
  Non-page files   |  34
  Static files     | 109
  Processed images |   0
  Aliases          |   0
  Sitemaps         |   1
  Cleaned          |   0

Total in 617 ms
Watching for changes in xxx\Labor\ftc-Webseite\Repos\Website Prototype Code\testsite\{content,themes}
Watching for config changes in xxx\Labor\ftc-Webseite\Repos\Website Prototype Code\testsite\config.toml, xxx\Labor\ftc-Webseite\Repos\Website Prototype Code\testsite\themes\website-layout\config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at //localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop

````
Diese "Sneak-Preview" finde ich ganz nützlich und empfehle sie jedem wärmstens.
Nebenbei wird so noch sichergestellt, dass die Seite tatsächlich gebaut werden
kann. Bei diversen Fehlern an der Technik verweigert hugo mit einer
entsprechenden Fehlermeldung. Das ist übrigens auch der Fall, wenn das
`git submodule update --init --recursive` vergessen wurde. Dann ist nämlich
`themes/website-layout/` noch leer.

hugo holt sich die Daten aus dem lokalen Fundus und zeigt daher alles so an,
wie es lokal vorhanden ist. Änderungen werden sofort dargestellt. Nun, es gibt
da ein paar Ausnahmen. hugo hakt gerne mal bei Änderungen in der
Seitenstruktur, oder ist es der Browser?
Hin und wieder hugo per CTRL-C (evtl. zweimal nötig!) stoppen, neu starten und
den Firefox-Cache tief mit CTRL-F5 ausleeren, hilft. Andere Browser haben
andere Tastenkombinationen dafür, wenn überhaupt.



## Wie man die neueste "Technik" abholt
Das sollte man tunlichst vor jedem Arbeitsbeginn machen!

An den Testdaten selbst wird es nicht oft Änderungen geben, was eventuell dazu
muss, hängt von dem expliziten Testfall ab. Trotzdem ist es nicht verkehrt das
Repo mit den Testinhalten aktuell zu halten.

Sind die Testdaten auf dem neuesten Stand, holt man sich die neueste Version
der Technik ins Haus. Das sieht dann zusammen so aus:
````
nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite (master)
$ git pull --recurse-submodules
Fetching submodule themes/website-layout
Already up-to-date.

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite (master)
$ cd themes/website-layout/

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite/themes/website-layout (master)
$ git fetch

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite/themes/website-layout (master)
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite/themes/website-layout (master)
````


## Wie man an der "Technik" arbeitet
Nachdem nun die lokalen Repos aktuell sind, muss man sich eventuell den
passenden Branch auswählen auf dem man arbeiten möchte. An dieser Stelle sind
wir bereits im Hauptverzeichnis der Testseite.

````
nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite (master)
$ git branch <arbeitsbranch>
$ git checkout <arbeitsbranch>
$ cd themes/website-layout/
$ git branch <was_zu_tun_ist>
$ git checkout <was_zu_tun_ist>
````
Wie man sieht wird das mit dem Branch zweimal gemacht. Einmal für das Repo
mit den Testdaten wobei der Branch da nicht zwingend nötig ist. Wenn man mag
kann da auch der originäre master-Branch verwendet werden.
Das zweite mal wird ein eigener Branch für das Subrepo mit der Technik
angelegt (bzw. ein vorhandener Branch weiterverwendet). Diese Trennung muss
man immer im Kopf behalten. Natürlich kann man die Kommandos auch gleich
mit absetzen wenn man die neueste Technik abholt.

Jedenfalls ist der Dateibaum unterhalb `themes` ein eigenes Repo mit den
eigentlichen Dateien der Technik. Im gitlab ist das unter
https://gitlab.com/ftcommunity/website-layout
zu finden. Die git bash achtet sehr genau darauf, wo sie da gerade im lokalen
Dateibaum unterwegs ist und setzt je nach Ort die Änderungen ins eine oder
andere Repo ab (zunächst die commits, später der push).

Die weitere Struktur der kompletten Seite ist in der Tech-Doc erklärt (die
zieht demnächst um, link fehlt jetzt hier deswegen).

Anlegen eines Branches, Dateien ändern, löschen, hinzufügen - kurz alles was
man so in einem Repo anstellt - funktioniert hier wie gewohnt.
Hat man seine Änderungen in der Technik (unterhalb `themes/website-layout/`)
fertig, werden sie in `themes/website-layout/` wie gewohnt comitted:
````
$ git add .
$ git commit -m "blabla"
````
Das trifft allerdings bis jetzt nur und ausschließlich das lokale Repo von 
https://gitlab.com/ftcommunity/website-layout
Erst ein `push` überführt die Änderungen ins Remote (gitlab-Server).
Wir arbeiten auf Branches und machen dort einen MR auf um die Änderungen
nochmal gegenzuprüfen und danach auf `master` zu übernehmen. Was auf `master`
ist (und nur das), ist produktiv wirksam!



## Wie man die neueste Technik verteilt
Um die Änderungen an der Technik nun auch dauerhaft bei den Testdaten oder
gar auf der Produktivseite  zu haben, muss noch einmal die jeweilige Seite,
genauer ihr Repo, angefasst werden.
Dazu muss vorher das commit und push (und ein MR) auf
https://gitlab.com/ftcommunity/website-layout
abgearbeitet sein!

Im Beispiel ziehe ich die neueste Version in die POC rein - schön brav auf nem
Branch. Man beachte die Ansagen der git bash wo und auf welchem branch wir
jeweils sind:
````
nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/website-hugo-poc (62-ft-pedia-downloadseite-konnte-rechts-neben-dem-ausgabenbild-das-beitragsverzeichnis-der-ausgabe-anzeigen)
$ cd themes/website-layout/

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/website-hugo-poc/themes/website-layout (master)
$ git fetch

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/website-hugo-poc/themes/website-layout (master)
$ git status
On branch master
Your branch is behind 'origin/master' by 2 commits, and can be fast-forwarded.
  (use "git pull" to update your local branch)
nothing to commit, working tree clean

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/website-hugo-poc/themes/website-layout (master)
$ git pull
Updating e9caa8a..53b5068
Fast-forward
 layouts/ftpedia/list.html | 54 ++++++++++++++++++++++++++++++++++++++++++++++-
 1 file changed, 53 insertions(+), 1 deletion(-)

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/website-hugo-poc/themes/website-layout (master)
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/website-hugo-poc/themes/website-layout (master)
$ cd ../..

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/website-hugo-poc (62-ft-pedia-downloadseite-konnte-rechts-neben-dem-ausgabenbild-das-beitragsverzeichnis-der-ausgabe-anzeigen)
$ git status
On branch 62-ft-pedia-downloadseite-konnte-rechts-neben-dem-ausgabenbild-das-beitragsverzeichnis-der-ausgabe-anzeigen
Your branch is up-to-date with 'origin/62-ft-pedia-downloadseite-konnte-rechts-neben-dem-ausgabenbild-das-beitragsverzeichnis-der-ausgabe-anzeigen'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   themes/website-layout (new commits)

no changes added to commit (use "git add" and/or "git commit -a")

$ git add .
$ git commit -m "Neue Technikversion"
$ git push
````
Erst nach diesen Zeilen ist die neueste Version der Technik in der Seite
final eingebaut und für den nächsten Nutzer im Remote-Repo verfügbar. Vorher
kommt die ältere Version der Technik auf die lokale Platte!

Sinngemäß ist dieses Prozedere nun für jede Seite zu wiederholen auf der die
Technik erneuert werden muss - insbesondere auch hier die Testseite.
(Zapfenkiller, Okt / 4 / 2019 - das ist soeben genau so verifiziert worden!)

Nun muss die Änderung noch vom Branch `62-ft-pedia-downloadseite-konnte-rechts-neben-dem-ausgabenbild-das-beitragsverzeichnis-der-ausgabe-anzeigen`
in den `master` überführt werden. Das geht wieder per MR übers gitlab.



## Wie man einen bestimmten Stand einspielt
Möchte man im Zuge eines MR prüfen, was da als Änderung vorgeschlagen wurde,
bietet es sich an die Testseite von hugo lokal servieren zu lassen. Dazu muss
hugo die gewünschten Daten vorfinden!
Tun wir mal so, als ob ein branch "aenderung_42" in website-layout existiert.
Zum Begutachten tut es der vorhandene master-Branch der Testinhalte.
````
$ cd <wo das Repo lokal liegt>
$ cd testsite
$ git pull --recurse-submodules
Fetching submodule themes/website-layout
Already up-to-date.

$ cd themes/website-layout
$ git checkout aenderung_42
Branch aenderung_42 set up to track remote branch aenderung_42 from origin.
Switched to a new branch 'aenderung_42'
$ hugo serve
````
Per Browser ist die gewünschte Änderung nun schön zu sehen bevor sie produktiv
werden darf - oder auch nicht.

Eventuell ist man noch nicht up-to-date und bekommt eine etwas andere Meldung
nach dem pull.

Sinngemäß gilt das auch für die Produktivseite. Und es gibt noch mehr
Möglichkeiten den Arbeitsablauf zu organisieren. Das oben sind funktionsfähige
Beispiele wie das funktioniert.



## Ein Beispiel
Gerade arbeite ich hier am README.md der Testseite. Die "Technik" ist auf dem
neuesten Stand. Beide Repo-Bereiche sind auf ihrem Branch `master` unterwegs:
````
nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite/themes (master)
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   ../README.md

no changes added to commit (use "git add" and/or "git commit -a")

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite/themes (master)
$ cd website-layout/

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite/themes/website-layout (master)
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean

nutzer@maschine MINGW64 xxx/Labor/ftc-Webseite/Repos/Website Prototype Code/testsite/themes/website-layout (master)
$ _
````
Man beachte bitte die beiden unterschiedlichen Ergebnisse des `git status`, je
nach Örtlichkeit! Entsprechend wird die branch info von der git bash
angepasst. Also Augen auf und volle Konzentration. Eventuell ist es geschickt
je Repo-Bereich mit einer eigenen git bash zu arbeiten.
