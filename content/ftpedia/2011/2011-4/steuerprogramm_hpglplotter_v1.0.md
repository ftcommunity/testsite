---
layout: "file"
hidden: true
title: "Steuerprogramm HP-GL-Plotter v1.0"
date: "2012-03-22T00:00:00"
file: "steuerprogramm_hpglplotter_v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/steuerprogramm_hpglplotter_v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/steuerprogramm_hpglplotter_v1.0.zip -->
Steuerprogramm für den in http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2011-4.pdf vorgestellten HP-GL-Plotter (siehe auch http://www.ftcommunity.de/categories.php?cat_id=2456). Beherrscht die HP-GL-Befehle IN, PU, PD, PA, PR (Bresenham-Algorithmus).