---
layout: "image"
title: "Auslagern"
date: "2017-09-30T11:52:18"
picture: "keksdrucker12.jpg"
weight: "12"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/46542
- /detailsb2d1-2.html
imported:
- "2019"
_4images_image_id: "46542"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46542 -->
keks im Trockenlager abholen, in die zweite Reihe oberhalb des Auswurfs fahren und nach unten fahren bis der Keks vom Greifer rutscht.