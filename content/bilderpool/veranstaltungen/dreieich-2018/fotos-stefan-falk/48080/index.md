---
layout: "image"
title: "LKW"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention161.jpg"
weight: "161"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48080
- /details19bb-2.html
imported:
- "2019"
_4images_image_id: "48080"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "161"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48080 -->
