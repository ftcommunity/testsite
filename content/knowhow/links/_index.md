---
title: "Links"
weight: 80
legacy_id:
- /wiki204c.html
- /php/wiki/38
imported:
- "2019"
---

### Weiterführende Links

* [Xbase++ (Win32) and Clipper (DOS) FischerTechnik Interface Drivers](http://members.chello.nl/r.nooteboom2/)
* [PolarFish](https://sourceforge.net/projects/polarfish/)
    (software to keep computing experimental, computing, plotter / scanner and the famous robot kit from the 80s alive)
* [Elektronik und Mikrocontroller von Burkhard Kainka](http://www.b-kainka.de/index.htm)
* [Elektronik-Kompendium](http://www.elektronik-kompendium.de/)
* [Kinder und Technik, 3D-Druck Arduino fischertechnik](http://www.kinder-technik.de/)

### Zubehör

* Reedkontakt 4mm: [Conrad](http://www.conrad.de/ce/de/product/503548/REEDSENSOR-ZYL-MS-213-3-10W-1S)
* Buchsen für ft-Stecker: [Conrad](http://www.conrad.de/ce/de/product/733628/)
* Kugellager 4mm: [Oppermann, Typ KLM 12](http://www.oppermann-electronic.de/html/body_mechanikteile.html)
* Magnet 4x2 mm: [Pollin](https://www.pollin.de/p/magnet-4x2-mm-441596)
* Reely 1:10 Monstertruck Komplettrad mit Retro-Felge Chrom und Traktor Reifen-Profil (50816C) ([Link zu Bild im Bilderpool](http://old.ftcommunity.de/details44ac.html?image_id=30513)): [Conrad](https://www.conrad.de/de/p/reely-1-10-monstertruck-komplettraeder-traktor-5-speichen-chrom-4-st-235763.html)
* Felgenmitnehmer-Set Schwarz (V21071L): [Conrad](https://www.conrad.de/de/p/reely-9-mm-1-10-kunststoff-felgenmitnehmer-12-mm-6-kant-9-mm-schwarz-1-set-232728.html)
* Getriebemotor EMG30 mit eingebautem Drehgeber: [Blog](https://www.mikrocontroller-elektronik.de/tag/motor/)


### Bezugsquellen für nützliche Technik

* [Conrad Electronic Shop](https://www.conrad.de/)
* [GHW](http://www.ghw-modellbau.de)
* [LEMO-SOLAR](https://lemo-solar.de)
* [OPITEC Bastelshop](https://de.opitec.com/opitec-web/st/Home)
* [Pollin Electronic](https://www.pollin.de/)
* [reichelt elektronik](https://www.reichelt.de/)
* [robototer-teile.de-Shop](http://roboter-teile.de)

Diese Listen sind nicht vollständig und können es nie werden. Sie stellen eine mehr oder weniger zufällige Auswahl dar und bedeuten keine Empfehlung für den Kauf bestimmter Produkte oder bei den genannten Anbietern.

