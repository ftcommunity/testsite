---
layout: "file"
hidden: true
title: "Konstruktion HP-GL-Plotter v1.1 (ft-designer)"
date: "2012-05-20T00:00:00"
file: "konstruktion_hpglplotter_v1.1.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/konstruktion_hpglplotter_v1.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/konstruktion_hpglplotter_v1.1.zip -->
3D-Konstruktionszeichnung (ft-Designer) für den in ft:pedia 4/2011 vorgestellten HP-GL-Plotter, inklusive Bauteilübersicht. Siehe auch unter [minimalistischer Präzisionsplotter](http://www.ftcommunity.de/categories.php?cat_id=2456)). 
