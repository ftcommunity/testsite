---
layout: "image"
title: "Großes Fahrzeug"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention154.jpg"
weight: "154"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48073
- /details10c3-2.html
imported:
- "2019"
_4images_image_id: "48073"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "154"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48073 -->
