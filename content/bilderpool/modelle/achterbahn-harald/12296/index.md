---
layout: "image"
title: "Achterbahn05.JPG"
date: "2007-10-23T19:58:05"
picture: "Achterbahn05.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12296
- /detailsa483.html
imported:
- "2019"
_4images_image_id: "12296"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T19:58:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12296 -->
Befestigungsvariante mit BS7,5. Zur Befestigung dient der eine links außen, die anderen sind als Schwert zum Schutz vor Entgleisung gedacht. Damit sie schön in der Richtung bleiben, steckt ein P-Schlauch oben drin.