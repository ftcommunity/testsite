---
title: "Know-how"
weight: 20
ordersectionsby: "weight"
---
Das "Gewusst Wie" ist oft unbezahlbar.
Hier findest Du es sogar kostenlos.

Für Teile ab 2018 finden sich 
wertvolle [technische Informationen](https://www.fischertechnik.de/de-de/suche?searchKeyword=datenblatt&tab=1)
direkt bei [fischertechnik](https://www.fischertechnik.de).

Über ältere Teile wissen oft die Fans Bescheid und teilen ihr Wissen hier mit
der restlichen Welt.
Die Rubriken sollten Dich auf den richtigen Pfad leiten, falls die Suche nichts
passendes findet.

Viel Spass beim Stöbern.
